import React, { useState } from 'react';
import Close from '../../../assets/xmark.svg';
import Trash from '../../../assets/trash.svg';

export default function Modal({ item, onDelete }) {
  const [isOpen, setIsOpen] = useState(false);
  return (
    <>
      <img
        className="w-6 h-6 cursor-pointer"
        src={Trash}
        onClick={() => setIsOpen(true)}
      />
      {isOpen ? (
        <>
          <div className="absolute top-0 left-0 z-10 w-screen min-h-screen bg-black bg-opacity-50 flex justify-center items-center">
            <div className="bg-white rounded-lg z-30 p-5 relative w-1/4">
              <img
                className="absolute w-3 top-2 right-2 cursor-pointer"
                src={Close}
                onClick={() => setIsOpen(false)}
              />
              <h1 className="text-black text-center">Hapus {item} ?</h1>
              <div className="flex justify-center items-center mt-4">
                <button
                  className="bg-red-800 px-4 py-1 rounded-full text-white"
                  onClick={onDelete}
                >
                  Hapus
                </button>
              </div>
            </div>
          </div>
        </>
      ) : null}
    </>
  );
}
