import React, { useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import './style.css';

export default function Form() {
  const navigate = useNavigate();
  const [name, setName] = useState('');
  const [price, setPrice] = useState(0);
  const [description, setDescription] = useState('');
  const [category, setCategory] = useState('');
  const [hasExpiry, setHasExpiry] = useState(false);
  const [expiryDate, setExpiryDate] = useState('');
  const [image, setImage] = useState('');

  const onSave = () => {
    const newItem = {
      id: 'sku' + Math.floor(1000 + Math.random() * 9000),
      name,
      price,
      image,
      description,
      isDeleted: false,
      category,
      expiryDate: hasExpiry ? expiryDate : null,
    };
    const isEmpty = Object.values(newItem).every((i) => i !== '');
    if (!isEmpty) {
      alert('Please fill all the fields');
    } else {
      console.log(newItem);
      return newItem;
    }
  };

  const handleSave = () => {
    const newItem = onSave();
    navigate('/products', { state: newItem });
  };

  const uploadImage = async (e) => {
    const file = e.target.files[0];
    const base64 = await convertBase64(file);
    setImage(base64);
  };

  const convertBase64 = (file) => {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => {
        resolve(reader.result);
      };
      reader.onerror = (error) => {
        reject(error);
      };
    });
  };

  return (
    <form action="" className="form--text">
      <div className="input-wrapper">
        <div className="input-field">
          <div className="row">
            {/* <InputName /> */}
            <div className="input-group">
              <label htmlFor="name">Product name</label>
              <input
                id="name"
                name="name"
                placeholder="Enter product name"
                type="text"
                onChange={(e) => setName(e.target.value)}
              />
            </div>
            {/* <InputPrice /> */}
            <div className="input-group">
              <label htmlFor="price">Product price</label>
              <input
                id="price"
                name="price"
                pattern="[0-9]+"
                placeholder="Enter product price"
                type="number"
                onChange={(e) => setPrice(e.target.value)}
              />
            </div>
            {/* <SelectCategory /> */}
            <div className="input-group">
              <label htmlFor="category">Product category</label>
              <select
                id="category"
                name="category"
                onChange={(e) => setCategory(e.target.value)}
              >
                <option value="">Select Category</option>
                <option value="Ready">Ready</option>
                <option value="Barang Bekas">Barang Bekas</option>
                <option value="Pre-Order">Pre-Order</option>
              </select>
            </div>
          </div>
          {/* <InputDescription /> */}
          <div className="input-group">
            <label htmlFor="description">Product Description</label>
            <textarea
              id="description"
              name="description"
              placeholder="Enter product description"
              onChange={(e) => setDescription(e.target.value)}
            />
          </div>
          {/* <HasExpiry /> */}
          <div className="check">
            <input
              id="expiry"
              name="expiry"
              type="checkbox"
              onChange={() => setHasExpiry(!hasExpiry)}
            />
            <label htmlFor="expiry">Product has expiry date</label>
          </div>
          {/* <ExpiryDate /> */}
          <div className="input-group">
            <label htmlFor="expiry-date">Expiry Date</label>
            {hasExpiry ? (
              <input
                id="expiry-date"
                name="expiry-date"
                type="date"
                onChange={(e) => setExpiryDate(e.target.value)}
              />
            ) : (
              <input
                id="expiry-date"
                name="expiry-date"
                type="date"
                onChange={(e) => setExpiryDate(e.target.value)}
                disabled
              />
            )}
          </div>
        </div>
        <div className="input-right-wrapper">
          <div className="preview">
            {image === '' ? <span>Product Image</span> : <img src={image} />}
          </div>
          <div className="form-image">
            <input
              type="file"
              className="btn-upload"
              onChange={(e) => uploadImage(e)}
            ></input>
          </div>
          <div className="btn-wrapper">
            {/* <Button /> */}
            <div>
              <Link to="/products">
                <button className="btn-cancel">Cancel</button>
              </Link>
              <button className="btn-save" onClick={handleSave}>
                Save
              </button>
            </div>
          </div>
        </div>
      </div>
    </form>
  );
}
