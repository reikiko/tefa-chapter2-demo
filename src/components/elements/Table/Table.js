import React from 'react';
import PropTypes from 'prop-types';

export default function Table({ items, columns, rows }) {
  return (
    <div className="-mx-4 sm:-mx-8 px-4 sm:px-8 max-h-[30rem] overflow-x-auto">
      <div className="inline-block min-w-full overflow-clip">
        <table className="w-full leading-normal text-xs">
          <thead>
            <tr>
              {columns.map((i, idx) => {
                return (
                  <th
                    key={idx}
                    scope="col"
                    className="font-medium text-[#9CA3AF] px-4 py-5 text-left bg-[#F8FAFC]"
                  >
                    {i}
                  </th>
                );
              })}
            </tr>
          </thead>
          <tbody>{items.map(rows)}</tbody>
        </table>
      </div>
    </div>
  );
}
Table.propTypes = {
  columns: PropTypes.array.isRequired,
  items: PropTypes.array.isRequired,
  rows: PropTypes.func.isRequired,
};
