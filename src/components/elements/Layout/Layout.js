import React from 'react';
import Header from './Header';
import Sidebar from './Sidebar';

export default function Layout({ children }) {
  return (
    <>
      <div className="relative flex min-h-screen">
        <Sidebar />
        <div className="w-full h-screen bg-slate-100">
          <Header />
          <div className="p-6">{children}</div>
        </div>
      </div>
    </>
  );
}
