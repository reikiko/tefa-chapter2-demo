import React from 'react';
import { NavLink } from 'react-router-dom';
import Logo from '../../../../assets/logo.svg';
import product from '../../../../assets/product.svg';
import user from '../../../../assets/user.svg';

export default function Sidebar() {
  const items = [
    { name: 'Products', icon: product, link: '/products' },
    { name: 'Users', icon: user, link: '/users' },
  ];
  return (
    <div className="w-80 bg-[#1E293B] px-6 py-4 max-h-screen">
      <img src={Logo} alt={'Logo'} />
      <h2 className="uppercase text-[#64748B] mt-10">Pages</h2>
      <div className="mt-3 flex flex-col gap-y-1">
        {items.map((item, index) => (
          <NavLink
            to={item.link}
            key={index}
            className={({ isActive }) =>
              isActive ? 'bg-[#0F172A] rounded-md' : ''
            }
          >
            <div className="flex items-center py-2 px-3 hover:bg-[#141e33] rounded-md">
              <img src={item.icon} alt={item.icon} />
              <p className="ml-3">{item.name}</p>
            </div>
          </NavLink>
        ))}
      </div>
    </div>
  );
}
