import React from 'react';
import message from '../../../../assets/message.svg';
import exclamation from '../../../../assets/exclamation.svg';
import logo2 from '../../../../assets/Logo-2.svg';
import arrowdown from '../../../../assets/arrowdown.svg';
import search from '../../../../assets/search.svg';

export default function Header() {
  const items = [{ icon: search }, { icon: message }, { icon: exclamation }];
  return (
    <div className="bg-white text-[#475569] flex justify-end p-4">
      {items.map((item, index) => {
        return (
          <div
            key={index}
            className="w-8 h-8 flex justify-center items-center mr-3"
          >
            <img src={item.icon} alt={item.icon} />
          </div>
        );
      })}
      <div className="bg-slate-300 w-0.5 mr-3"></div>
      <div className="flex items-center">
        <div className="w-8 h-8 flex justify-center items-center mr-2">
          <img src={logo2} alt={'Logo'} />
        </div>
        <p className="text-[#475569] mr-6">Acne</p>
        <img src={arrowdown} alt={'More'} />
      </div>
    </div>
  );
}
