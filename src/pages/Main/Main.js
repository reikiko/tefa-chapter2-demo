import React from 'react';

export default function Main() {
  return (
    // <main className={styles.root}>
    //   <div>
    //     <h1 className="text-red-500 text-3xl font-bold">Telkom TEFA</h1>
    //     <p className="text-black">{subtitles[idx]}</p>
    //     <button className="text-black" onClick={onClick}>
    //       next
    //     </button>
    //   </div>
    // </main>
    <main>
      <div className="flex h-96 justify-center items-center">
        <h1 className="text-red-500 text-3xl font-bold">Telkom TEFA</h1>
      </div>
    </main>
  );
}
