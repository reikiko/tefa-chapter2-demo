import React, { useState, useEffect } from 'react';
import product from './product2.json';
import Table from '../../components/elements/Table';
import { Link, useLocation } from 'react-router-dom';
import Modal from '../../components/elements/Modal';
import dateFormat from 'dateformat';
import './style.css';

export default function Product() {
  const { state } = useLocation();
  const [data, setData] = useState(product);
  console.log(state);
  useEffect(() => {
    if (state !== null) {
      setData([state, ...data]);
      // console.log('yess  ');
    }
  }, [state]);

  const columns = [
    'Product Name',
    'Description',
    'Product Price',
    'Category',
    'Expiry Date',
    'ACTION',
  ];

  const checkDate = (i) => {
    return new Date(i.expiryDate) > new Date();
  };

  const numberWithDot = (number) => {
    return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.');
  };

  const changeDateFormat = (date) => {
    return dateFormat(date, 'd mmmm yyyy	');
  };

  const [category, setCategory] = useState('');
  const onChangeSelect = (e) => {
    setCategory(e.target.value);
    const filteredData =
      !e.target.value && !checked
        ? product
        : !e.target.value && checked
        ? product.filter((i) => checkDate(i) || i.expiryDate === null)
        : e.target.value && !checked
        ? product.filter((i) => i.category === e.target.value)
        : checked && e.target.value
        ? product.filter(
            (i) =>
              (checkDate(i) || i.expiryDate === null) &&
              i.category === e.target.value
          )
        : product;
    setData(filteredData);
  };

  const [checked, setChecked] = useState('');
  const onChangeCheck = (e) => {
    setChecked(e.target.checked);
    const filteredData =
      checked && category === ''
        ? product
        : !checked && category !== ''
        ? product.filter(
            (i) =>
              (checkDate(i) || i.expiryDate === null) && i.category === category
          )
        : checked && category !== ''
        ? product.filter((i) => i.category === category)
        : !checked && category === ''
        ? product.filter((i) => checkDate(i) || i.expiryDate === null)
        : product;
    setData(filteredData);
  };

  const onDelete = (id) => {
    const newData = data.map((i) => {
      if (i.id === id) {
        i.isDeleted = true;
      }
      return i;
    });
    setData(newData);
    console.log(newData);
  };

  const rowsData = (i, idx) => {
    return (
      !i.isDeleted &&
      (checkDate(i) || i.expiryDate === null ? (
        <tr key={idx}>
          <td className="px-5 py-4 font-medium text-gray-900">
            <Link to={`/products/${i.id}`}>
              <div className="flex items-center">
                <div className="flex-shrink-0 w-12 h-12 rounded-full relative overflow-hidden">
                  <img
                    className="inset-y-0 bg-cover absolute m-auto"
                    src={i.image}
                  />
                </div>
                <div className="ml-3">
                  <p className="whitespace-nowrap">{i.name}</p>
                </div>
              </div>
            </Link>
          </td>
          <td className="max-w-sm text-gray-900 font-light px-5 py-4">
            {i.description}
          </td>
          <td className="font-bold text-green-600 px-5 py-4">
            Rp{numberWithDot(i.price)}
          </td>
          <td className="max-w-sm text-gray-900 font-light px-5 py-4">
            {i.category}
          </td>
          <td className="max-w-sm text-gray-900 font-light px-5 py-4">
            {i.expiryDate !== null ? changeDateFormat(i.expiryDate) : ''}
          </td>
          <td className="px-6 py-4">
            <Modal item={i.name} onDelete={() => onDelete(i.id)} />
          </td>
        </tr>
      ) : (
        <tr key={idx}>
          <td className="px-5 py-4 font-medium text-gray-900 opacity-30">
            <Link to={`/products/${i.id}`}>
              <div className="flex items-center">
                <div className="flex-shrink-0 w-12 h-12">
                  <img className="w-full h-full rounded-full" src={i.image} />
                </div>
                <div className="ml-3">
                  <p className="whitespace-nowrap">{i.name}</p>
                </div>
              </div>
            </Link>
          </td>
          <td className="max-w-sm text-gray-900 font-light px-5 py-4 opacity-30">
            {i.description}
          </td>
          <td className="font-bold text-green-600 px-5 py-4 opacity-30">
            Rp{numberWithDot(i.price)}
          </td>
          <td className="max-w-sm text-gray-900 font-light px-5 py-4 opacity-30">
            {i.category}
          </td>
          <td className="max-w-sm text-gray-900 font-light px-5 py-4 opacity-30">
            {i.expiryDate !== null ? changeDateFormat(i.expiryDate) : ''}
          </td>
          <td className="px-6 py-4">
            <Modal item={i.name} onDelete={() => onDelete(i.id)} />
          </td>
        </tr>
      ))
    );
  };

  return (
    <main className="bg-white rounded-md">
      <div className="flex flex-row">
        <h1 className="text-gray-900 font-semibold px-5 py-7 ml-5 w-[35%]">
          Products
        </h1>
        <div className="flex justify-end items-center text-sm">
          <div className="check select-none mr-[44px]">
            <input
              id="expiry"
              name="expiry"
              type="checkbox"
              onChange={onChangeCheck}
            />
            <label htmlFor="expiry">Hide expired product</label>
          </div>
          <select
            onChange={onChangeSelect}
            value={category}
            className="mr-[44px] h-fit"
          >
            <option value="">All Category</option>
            <option value="Ready">Ready</option>
            <option value="Barang Bekas">Barang Bekas</option>
            <option value="Pre-Order">Pre-Order</option>
          </select>
          <Link
            to="/products/new"
            className="w-fit h-fit px-[28px] py-[8px] bg-[#6366f1] text-white rounded-md"
          >
            Add New Product
          </Link>
        </div>
      </div>
      <div className="bg-slate-300 h-0.5"></div>
      <div className="px-5 py-4 text-sm">
        <Table columns={columns} items={data} rows={rowsData} />
      </div>
    </main>
  );
}
