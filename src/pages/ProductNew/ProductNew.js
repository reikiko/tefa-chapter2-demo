import React from 'react';
import './style.css';
import Form from '../../components/elements/Form';

export default function ProductNew() {
  return (
    <main className="wrapper">
      <div className="head">
        <h1 className="title">
          <span className="title--purple">Products</span> / Add New Product
        </h1>
      </div>
      <div className="border"></div>
      <Form />
    </main>
  );
}
