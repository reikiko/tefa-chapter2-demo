import React from 'react';
import { useParams } from 'react-router-dom';

export default function ProductDetail() {
  const params = useParams();
  return (
    <main>
      <div className="flex h-96 justify-center items-center">
        <h1 className="text-red-500 text-3xl font-bold">
          Detail Product {params.id}
        </h1>
      </div>
    </main>
  );
}
