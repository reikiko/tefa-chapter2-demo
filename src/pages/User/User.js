import React, { useState } from 'react';
import user from './user.json';
import Table from '../../components/elements/Table';
import Trash from '../../assets/trash.svg';
import Modal from '../../components/elements/Modal';

export default function User() {
  const columns = ['USERNAME', 'EMAIL', 'ACTION'];
  const [isOpen, setIsOpen] = useState(false);
  const [data, setData] = useState();
  const closeModal = () => {
    setIsOpen(false);
  };
  const openModal = (id) => {
    setIsOpen(true);
    setData(user.filter((user) => user.id === id));
  };

  const rowsData = (i, idx) => {
    return (
      <tr key={idx}>
        <td className="px-6 py-4 text-md font-medium text-gray-900">
          <div className="flex items-center">
            <div className="flex-shrink-0 w-12 h-12">
              <img
                className="w-full h-full rounded-full"
                src={i.profilePicture}
              />
            </div>
            <div className="ml-3">
              <p className="whitespace-nowrap">{i.username}</p>
            </div>
          </div>
        </td>
        <td className="max-w-sm text-md text-gray-900 font-light px-6 py-4">
          {i.email}
        </td>
        <td className="px-6 py-4">
          <img
            className="w-6 h-6 cursor-pointer"
            src={Trash}
            onClick={() => openModal(i.id)}
          />
        </td>
      </tr>
    );
  };

  return (
    <main className="bg-white rounded-md">
      <h1 className="text-gray-900 font-semibold px-5 py-7 ml-5">Users</h1>
      <div className="bg-slate-300 h-0.5"></div>
      <div className="px-5 py-4 text-sm">
        <Table columns={columns} items={user} rows={rowsData} />
        <Modal isOpen={isOpen} closeModal={closeModal} item={data} />
      </div>
    </main>
  );
}
